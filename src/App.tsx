import * as React from "react";
import { render } from "react-dom";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import CustomRouter from "./CustomRouter";

render(<CustomRouter />, document.getElementById("main"));
