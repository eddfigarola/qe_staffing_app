import * as React from "react";
import { render } from "react-dom";
var listName = "bench_attendance";
import Button from "@material-ui/core/Button";
import Map from "../Components/Map";

export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.sendupdates = this.sendupdates.bind(this);
    this.showPosition = this.showPosition.bind(this);
    this.state = { latitude: 0, longitude: 0 };
  }

  state = {
    latitude: 0,
    longitude: 0
  };

  sendupdates() {
    let latitude = this.state.latitude;
    var longitude = this.state.longitude;

    var location =
      "https://ts.accenture.com/sites/Monterrey-Share/Operations/IHG/";
    var command = "New"; // "Update" or "New"
    var fieldnames = ["ID", "Title", "Longitude"]; //array with the fieldnames
    var fieldvalues = ["1", latitude, longitude]; //array with the values

    var updatesvar;
    for (let x = 0; x < fieldnames.length; x++) {
      updatesvar =
        updatesvar +
        '<Field Name="' +
        fieldnames[x] +
        '">' +
        fieldvalues[x] +
        "</Field>";
    }

    var batchvar =
      '<Batch OnError="Continue" ListVersion="0"><Method ID="1" Cmd="' +
      command +
      '">' +
      updatesvar +
      "</Method></Batch>";

    var soapEnv =
      '<?xml version="1.0" encoding="utf-8"?>' +
      '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
      "<soap:Body>" +
      '<UpdateListItems xmlns="http://schemas.microsoft.com/sharepoint/soap/">' +
      "<listName>" +
      listName +
      "</listName>" +
      "<updates>" +
      batchvar +
      "</updates>" +
      "</UpdateListItems>" +
      "</soap:Body>" +
      "</soap:Envelope>";

    fetch(location + "/_vti_bin/Lists.asmx", {
      method: "POST",
      headers: new Headers({
        SOAPAction:
          "http://schemas.microsoft.com/sharepoint/soap/UpdateListItems",
        "Content-Type": "text/xml; charset=utf-8",
        Accept: "*/*",
        "Accept-Language": "en-GB",
        "Accept-Encoding": "gzip, deflate",
        Connection: "Keep-alive"
      }),
      body: soapEnv
    });
  }

  componentDidMount() {
    this.getLocation();
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.showPosition);
    } else {
      console.log("geo location not supported");
    }
  }
  showPosition(position) {
    this.updateState(position.coords.latitude, position.coords.longitude);
  }
  updateState(x, y) {
    this.setState({ latitude: x, longitude: y });
  }
  render() {
    return (
      <div>
        <h1>Bienvenido</h1>
        <Button
          onClick={() => this.sendupdates()}
          variant="contained"
          color="primary"
        >
          Registrar Entrada
        </Button>

        <div id="map" style={{ height: "100vh", width: "100%" }}>
          <Map
            latitude={this.state.latitude}
            longitude={this.state.longitude}
          />
        </div>
      </div>
    );
  }
}
