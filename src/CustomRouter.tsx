import * as React from "react";
import { Component } from "react";
import Register from "./Pages/Register";
import AttendanceList from "./Pages/AttendanceList";
export interface CustomRouterProps {}

export interface CustomRouterState {}

class CustomRouter extends React.Component {
  constructor(props) {
    super(props);
    const page = this.getUrlParam("page", "register");
  }

  page = "register";
  componentWillMount() {
    this.page = this.getUrlParam("page", "register");
  }

  getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(
      /[?&]+([^=&]+)=([^&]*)/gi,
      function(m, key, value) {
        vars[key] = value;
      }
    );
    return vars;
  }

  getUrlParam(parameter, defaultvalue) {
    var urlparameter = defaultvalue;
    if (window.location.href.indexOf(parameter) > -1) {
      urlparameter = this.getUrlVars()[parameter];
    }
    return urlparameter;
  }

  render() {
    console.log(this.page);
    let component = <Register />;
    if (this.page != "register") {
      component = <AttendanceList />;
    }
    return component;
  }
}

export default CustomRouter;
