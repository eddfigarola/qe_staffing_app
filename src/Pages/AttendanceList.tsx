import * as React from "react";
import { render } from "react-dom";
var listName = "bench_attendance";
import Button from "@material-ui/core/Button";
import Map from "../Components/Map";

export default class AttendanceList extends React.Component {
  constructor(props) {
    super(props);
    this.getdata = this.getdata.bind(this);
    this.showPosition = this.showPosition.bind(this);
    this.state = { latitude: 0, longitude: 0 };
  }

  state = {
    latitude: 0,
    longitude: 0
  };

  getdata() {
    let latitude = this.state.latitude;
    var longitude = this.state.longitude;

    var location =
      "https://ts.accenture.com/sites/Monterrey-Share/Operations/IHG";

    var soapEnv =
      '<?xml version="1.0" encoding="utf-8"?>' +
      '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
      "<soap:Body>" +
      '<GetListItems xmlns="http://schemas.microsoft.com/sharepoint/soap/">' +
      "<listName>" +
      listName +
      "</listName>" +
      "</GetListItems>" +
      "</soap:Body>" +
      "</soap:Envelope>";

    fetch(location + "/_vti_bin/Lists.asmx", {
      method: "POST",
      headers: new Headers({
        SOAPAction: "http://schemas.microsoft.com/sharepoint/soap/GetListItems",
        "Content-Type": "text/xml; charset=utf-8",
        Accept: "*/*",
        "Accept-Language": "en-GB",
        "Accept-Encoding": "gzip, deflate",
        Connection: "Keep-alive"
      }),
      body: soapEnv
    })
      .then(res => {
        return res.text();
      })
      .then(text => {
        console.log(text);
        let items = text.split("z:row");
        console.log(items);
      });
  }

  componentDidMount() {
    this.getdata();
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.showPosition);
    } else {
      console.log("geo location not supported");
    }
  }
  showPosition(position) {
    this.updateState(position.coords.latitude, position.coords.longitude);
  }
  updateState(x, y) {
    this.setState({ latitude: x, longitude: y });
  }
  render() {
    return (
      <div>
        <h1>Dashboard</h1>
      </div>
    );
  }
}
