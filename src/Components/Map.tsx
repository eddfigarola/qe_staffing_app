import * as React from 'react';
import { Component } from 'react';
import { useState, useEffect } from 'react';
import ReactMapGL, { Marker }  from 'react-map-gl';

import { Icon } from '@material-ui/core';


export default class Map extends React.Component{
    constructor(props) {
        super(props);
        this.state = {width: 500, height: 400, latitude: 25.000, longitude: -100.22, zoom: 13}
      }

      state = {
        width: 500, height: 400, latitude: 25.000, longitude: -100.22, zoom: 13
      }

     
 
  
  render(){
     
  return (
    
    <ReactMapGL  mapboxApiAccessToken={"pk.eyJ1IjoiZWRmaWdhcm9sYSIsImEiOiJjazZ6YTA0YzYwemVnM2xydnlyaW4yZHZpIn0.aYzW2TZUXc3EkL7tA7kC6w"}
      width={this.state.width}
      height={this.state.height}
      latitude={this.props.latitude}
      longitude={this.props.longitude}
      zoom={13}
     >
      <Marker longitude={this.props.longitude} latitude={this.props.latitude}>
  <Icon color={"primary"}>location_on</Icon>
      </Marker>
    </ReactMapGL>

  );
}
}